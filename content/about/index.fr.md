+++
title = "À propos"
description = ""
date = "2019-02-28"
aliases = ["about", "about-me"]
author = "Andy Costanza"
reward = false
postDate = false
readingTime = false
+++

Développeur Java depuis 2001, les applications web monolytique, les architectures en microservices, ainsi que la mise en place de processus d’intégration et de déploiement continu d'applications font mon quotidien.

Pour en savoir d'avantage sur mon expérience professionnel, vous pouvez [consulter mon CV](https://andycostanza.gitlab.io/cv).
