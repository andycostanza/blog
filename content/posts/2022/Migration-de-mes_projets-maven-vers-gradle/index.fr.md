+++
title = "Migration de mes projets Maven vers Gradle"
tags = ["maven","java","gradle"]
categories = ["Dev"]
author = "Andy Costanza"
comments = true
layout = "post"
date = "2022-07-05T11:15:50+01:00"
+++
## Pourquoi? 
Changement de job, changement de culture, changement de techno, après plus de 10 ans de bons et loyaux services, je vais remplacer maven par gradle pour la construction de mes applications. 

Pourquoi changer alors que ca fonctionne bien?

Gradle offre deux avantages majeurs par rapport à Maven :
- [le build incrémental](https://blog.gradle.org/introducing-incremental-build-support)
- [les performances](https://gradle.org/gradle-vs-maven-performance/)

![](https://gradle.org/images/gradle-vs-maven.gif)

Il est difficile de revenir en arrière après ça et si vous n'êtes toujours pas convaincu, Gradle en a encore sous le coude avec [ses autres atouts](https://gradle.org/features/)

## Comment?
Il est assez simple de migrer un projet Maven vers Gradle.
1. [Installez Gradle](https://gradle.org/install/)
1. Utilisez la commande `gradle init` à la racine de son projet Maven
```bash
❯ gradle init

Found a Maven build. Generate a Gradle build from this? (default: yes) [yes, no] yes

Select build script DSL:
  1: Groovy
  2: Kotlin
Enter selection (default: Groovy) [1..2] 1

Generate build using new APIs and behavior (some features may change in the next minor release)? (default: no) [yes, no] yes

> Task :init
Maven to Gradle conversion is an incubating feature.
Get more help with your project: https://docs.gradle.org/7.4.2/userguide/migrating_from_maven.html

BUILD SUCCESSFUL in 12s
2 actionable tasks: 1 executed, 1 up-to-date
```

Si vous êtes derrière un proxy d'entreprise, vous risquez d'avoir le message d'erreur suivant:
```bash
> Task :init FAILED

FAILURE: Build failed with an exception.

* What went wrong:
Execution failed for task ':init'.
> Could not resolve all files for configuration ':detachedConfiguration1'.
   > Could not resolve org.apache.maven:maven-core:3.6.3.
     Required by:
         project :
      > Could not resolve org.apache.maven:maven-core:3.6.3.
         > Could not get resource 'https://repo.maven.apache.org/maven2/org/apache/maven/maven-core/3.6.3/maven-core-3.6.3.pom'.
            > Could not HEAD 'https://repo.maven.apache.org/maven2/org/apache/maven/maven-core/3.6.3/maven-core-3.6.3.pom'.
               > Connect to repo.maven.apache.org:443 [repo.maven.apache.org/151.101.8.215] failed: connect timed out
   > Could not resolve org.apache.maven:maven-plugin-api:3.6.3.
     Required by:
         project :
      > Could not resolve org.apache.maven:maven-plugin-api:3.6.3.
         > Could not get resource 'https://repo.maven.apache.org/maven2/org/apache/maven/maven-plugin-api/3.6.3/maven-plugin-api-3.6.3.pom'.
            > Could not HEAD 'https://repo.maven.apache.org/maven2/org/apache/maven/maven-plugin-api/3.6.3/maven-plugin-api-3.6.3.pom'.
               > Connect to repo.maven.apache.org:443 [repo.maven.apache.org/151.101.8.215] failed: connect timed out
   > Could not resolve org.apache.maven:maven-compat:3.6.3.
     Required by:
         project :
      > Could not resolve org.apache.maven:maven-compat:3.6.3.
         > Could not get resource 'https://repo.maven.apache.org/maven2/org/apache/maven/maven-compat/3.6.3/maven-compat-3.6.3.pom'.
            > Could not HEAD 'https://repo.maven.apache.org/maven2/org/apache/maven/maven-compat/3.6.3/maven-compat-3.6.3.pom'.
               > Connect to repo.maven.apache.org:443 [repo.maven.apache.org/151.101.8.215] failed: connect timed out
```

Pour cela, il vous suffira de créer à la racine de votre projet, le fichier `gradle.properties` avec la configuration de votre proxy:
```java
#HTTP-PROXY
systemProp.http.proxyHost=www.somehost.org
systemProp.http.proxyPort=8080
systemProp.http.proxyUser=userid
systemProp.http.proxyPassword=password
#HTTPS-PROXY
systemProp.https.proxyHost=www.somehost.org
systemProp.https.proxyPort=8080
systemProp.https.proxyUser=userid
systemProp.https.proxyPassword=password
#NON-PROXY
systemProp.http.nonProxyHosts=*.nonproxyrepos.com|localhost
```

## Si vous avez encore un problème?
* Sur Ubuntu 20.04 et la version snap de Gradle 7.2, j'ai été dans l'impossibilité d'appliquer la configuration de mon serveur proxy. La seule solution a été d'installer un wrapper gradle récent `gradle wrapper --gradle-version 7.4.2` et ensuite de l'utiliser pour lancer la migration du projet Maven avec `./gradlew init` comme expliqué précédement.
* Impossible non plus d'utiliser votre repository d'entreprise style Nexus ou Artifactory pour faire la migration. J'ai essayé d'appliquer une configuration dans le fichier `init.gradle` mais sans succès. Il est nécessaire de sortir du réseau d'entreprise à travers son proxy pour pouvoir migrer son projet
